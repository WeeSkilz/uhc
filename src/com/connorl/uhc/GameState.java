package com.connorl.uhc;

/**
 * Created by Connor on 22/02/2015.
 */
public enum GameState {
    WAITING, STARTED, PVP, FINISHED
}
