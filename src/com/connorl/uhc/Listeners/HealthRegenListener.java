package com.connorl.uhc.Listeners;

import com.connorl.uhc.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityRegainHealthEvent;

/**
 * Created by Connor on 17/01/2015.
 */
public class HealthRegenListener implements Listener {

    public HealthRegenListener(Main instance) {
        instance.getServer().getPluginManager().registerEvents(this, instance);
    }

    @EventHandler
    public void onPlayerRegainHealth(EntityRegainHealthEvent event) {
        if (((event.getEntity() instanceof Player)) && (event.getRegainReason() == EntityRegainHealthEvent.RegainReason.SATIATED)) {
            event.setCancelled(true);
        }
    }
}
