package com.connorl.uhc.Listeners;

import com.connorl.uhc.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Connor on 27/02/2015.
 */
public class QuitListener implements Listener {
    public QuitListener(Main instance) {
        instance.getServer().getPluginManager().registerEvents(this, instance);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        e.setQuitMessage(null);
        if(Main.currentGame != null) {
            Main.currentGame.removePlayer(e.getPlayer());
        }
    }
}
