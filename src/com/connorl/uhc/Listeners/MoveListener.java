package com.connorl.uhc.Listeners;

import com.connorl.uhc.GameState;
import com.connorl.uhc.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * Created by Connor on 22/02/2015.
 */
public class MoveListener implements Listener {
    public MoveListener(Main instance) {
        instance.getServer().getPluginManager().registerEvents(this, instance);
    }

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        if(Main.currentGame != null) {
            if(Main.currentGame.getState() == GameState.WAITING) {
                e.setCancelled(true);
            }
        }
    }
}
